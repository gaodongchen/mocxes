<?php

/**
 * Mocxes
 * 
 * means ...
 */

include "includes/core.php";

$opts = getopt('i:o:');

$csvfile = $opts['i'] ?? '';
$dbfile = $opts['o'] ?? '';

if (empty($dbfile)) {
	log_error('db file is empty');exit;
} else {
	if (file_exists($dbfile)) {
		$db = new SQLite3($dbfile);
	} else {
		log_error('not found db file');exit;
	}
}

if (empty($csvfile)) {
	log_error('csv file is empty');exit;
} else {
	if (file_exists($csvfile)) {
		$handle = fopen($csvfile, 'r');
		while($row = fgetcsv($handle)) {
			$tasks[] = ['url' => $row[0]];
		}
		fclose($handle);
	} else {
		log_error('not found csv file');exit;
	}
}

$total_count = 0;

foreach($tasks as $task) {
	$url = $task['url'];
	$images = QL\QueryList::get($url)->find('img')->attrs('data-src');
	log_info(sprintf("form %s collected %s images", $url, count($images)));
	foreach($images as $image) {
		if (!empty($image)) {
			$sql = "select * from images where url='{$image}';";
			$ret = $db->query($sql);
			$row = $ret->fetchArray(SQLITE3_ASSOC);
			if (empty($row)) {
				$sql = "insert into images (url,updated_at,created_at) values ('{$image}',datetime('now'),datetime('now'));";
				$db->exec($sql);
				$total_count++;
			}
		}
	}
}

log_info(sprintf("save %s images to %s", $total_count, $dbfile));