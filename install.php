<?php
/**
 * 初始化应用
 */

include "includes/core.php";

$opts = getopt('o:');

$dbfile = $opts['o'] ?? '';

$db = new SQLite3($dbfile);

 $sql =<<<EOF
    CREATE TABLE images (
      id integer PRIMARY KEY NOT NULL,
      url VARCHAR(1000),
      created_at integer,
      updated_at integer
    )
 EOF;

 $db->exec($sql);
 log_info(sprintf('create db %s success', $dbfile));