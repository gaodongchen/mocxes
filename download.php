<?php
include "includes/core.php";

$opts = getopt('i:o:', ['referer:']);

$dbfile = $opts['i'] ?? '';
$storage = $opts['o'] ?? '';
$referer = $opts['referer'] ?? '';

if (empty($dbfile)) {
	log_error('db file is empty');exit;
} else {
	if (file_exists($dbfile)) {
		$db = new SQLite3($dbfile);
	} else {
		log_error('not found db file');exit;
	}
}

if (!file_exists($storage)) {
	mkdir($storage, 0775, true);
}

$ret = $db->query('select * from images');
$client = new \GuzzleHttp\Client();
while($row = $ret->fetchArray(SQLITE3_ASSOC)) {
    $url = $row['url'];
    $filename = url_file_name($url);
    $to = $storage.'/'.$filename;
	if (!file_exists($to)) {
		$response = $client->request('get', $url, [
			'sink' => $to,
			'headers' => [
				'Referer' => $referer,
				'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.16; rv:84.0) Gecko/20100101 Firefox/84.0'
			]
		]);
		if ($response->getStatusCode() == 200) {
			log_info(sprintf('save %s to %s success', $url, $to));
		}else{
			log_error(sprintf('save %s to %s fail', $url, $to));
		}
	}
}

function url_file_name(string $url): string {
	$parts = parse_url($url);
	$parts = pathinfo($parts['path']);
	return $parts['basename'];
}