<?php

define("APPPATH", dirname(__FILE__, 2));
define("DBFILE", APPPATH . "/resource/mocxes.db");

include APPPATH . "/vendor/autoload.php";

function line(string $message): void {
    echo date('c') . ' ' . $message, PHP_EOL;
}

function log_info($message): void {
    line("[INFO] ".$message);
}

function log_error($message): void {
    line("[ERROR] ".$message);
}